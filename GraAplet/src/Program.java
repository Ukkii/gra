import java.applet.Applet;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;


public class Program extends Applet implements KeyListener
{

	int rozmiar1 = 14*40;
	int rozmiar2 = 10*40;
	static Applet applet;
	Quests quests = new Quests();
	Timer timer = new Timer();
	Image bufor;
	Graphics bg;
	static int stan = 1;
	
	Image skrzynka;
	Image ramka;
	Image ludek;
	
	
	public void init()
	{
		applet = this;
		applet.addKeyListener(this);
		applet.setSize(rozmiar1,rozmiar2);
		applet.setBackground(Color.LIGHT_GRAY);
		bufor =createImage(rozmiar1,rozmiar2);
		bg = bufor.getGraphics();
		timer.scheduleAtFixedRate(quests,10,10);
		
		
//		skrzynka = getImage(getDocumentBase(),"obrazy/Skrzynka.JPEG");
//		ramka = getImage(getDocumentBase(),"obrazki/Ramka.JPEG");
//		ludek = getImage(getDocumentBase(),"obrazki/Ludek.JPEG");
		
		quests.zrobPlansze();
	}
	public void update(Graphics g)
	{
		bg.clearRect(0,0,rozmiar1,rozmiar2);
		paint(bg);
		g.drawImage(bufor,0,0,applet);
	}
	public void paint(Graphics g)
	{
		switch(stan)
		{
		case 1:
			rysujPlansze(g);
			break;
		case 2:
			koniec(g);
			break;
		}
	}
	public void koniec(Graphics g)
	{
		applet.setBackground(Color.LIGHT_GRAY);
	}
	public void rysujPlansze(Graphics g)
	{
		for(int i=0;i<quests.plansza.length;i++)
		{
			for(int j=0;j<quests.plansza[0].length;j++)
			{
				if(quests.wyjscie[i][j]==2)
				{
					g.setColor(Color.BLUE);
					g.fillRect(40*j, 40*i,40,40);
				}
				switch(quests.plansza[i][j])
				{
				case 1:
					g.setColor(Color.RED);
					g.fillRect(40*j, 40*i,40,40);
					break;
//				case 2:
//					g.setColor(Color.BLUE);
//					g.fillRect(40*j, 40*i,40,40);
//					break;
				case 3:
					g.setColor(Color.GREEN);
					g.fillOval(40*j, 40*i,40,40);
				    break;
				case 4:
					g.setColor(Color.BLACK);
					g.fillOval(40*j, 40*i,40,40);
				    break; 
				    
				}
				
			}
			
			
		}
	}
	public void keyPressed(KeyEvent arg0) 
	{
		switch(arg0.getKeyCode())
		{
		case 37:
			quests.ruszaj('l');
			break;
		case 38:
			quests.ruszaj('g');
			break;
		case 39:
			quests.ruszaj('p');
			break;
		case 40:
			quests.ruszaj('d');
			break;
		}
		
	}
	public void keyReleased(KeyEvent arg0) 
	{
		// TODO Auto-generated method stub
		
	}
	public void keyTyped(KeyEvent arg0) 
	{
		// TODO Auto-generated method stub
		
	}
	
}
